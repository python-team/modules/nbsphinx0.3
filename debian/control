Source: nbsphinx0.3
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Jerome Benoit <calculus@rezozer.net>
Build-Depends: debhelper-compat (= 11),
Build-Depends-Indep: dh-python,
                     libjs-mathjax,
                     pandoc,
                     python-all,
                     python-docutils,
                     python-entrypoints,
                     python-ipykernel,
                     python-jupyter-client,
                     python-nbconvert (>=5.4-2),
                     python-nbformat,
                     python-setuptools,
                     python-sphinx,
                     python3-ipykernel,
                     rdfind,
                     symlinks
Standards-Version: 4.4.1
Homepage: https://nbsphinx.rtfd.org/
Vcs-Git: https://salsa.debian.org/python-team/packages/nbsphinx0.3.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/nbsphinx0.3

Package: python-nbsphinx
Architecture: all
Depends: ${misc:Depends}, ${python:Depends}
Suggests: python-nbsphinx0.3-doc (= ${source:Version})
Description: Jupyter Notebook Tools for Sphinx -- Python2
 nbsphinx is a Sphinx extension that provides a source parser for *.ipynb
 files. Custom Sphinx directives are used to show Jupyter Notebook code cells
 (and of course their results) in both HTML and LaTeX output.
 Un-evaluatednotebooks, i.e., notebooks without stored output cells, will be
 automatically executed during the Sphinx build process.
 .
 This package installs the library for Python 2.

Package: python-nbsphinx0.3-doc
Section: doc
Architecture: all
Depends: libjs-mathjax, ${misc:Depends}, ${sphinxdoc:Depends}
Suggests: www-browser
Enhances: python-nbsphinx (= ${source:Version})
Description: Jupyter Notebook Tools for Sphinx -- Python2 - doc
 nbsphinx is a Sphinx extension that provides a source parser for *.ipynb
 files. Custom Sphinx directives are used to show Jupyter Notebook code cells
 (and of course their results) in both HTML and LaTeX output.
 Un-evaluatednotebooks, i.e., notebooks without stored output cells, will be
 automatically executed during the Sphinx build process.
 .
 This is the common documentation package.
